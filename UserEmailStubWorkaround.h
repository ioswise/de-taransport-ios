//
//  UserEmailStubWorkaround.h
//  DeTransport
//
//  Created by  Igor Cherepanov on 11/14/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserEmailStubWorkaround : NSObject
@property(nonatomic, readonly) NSString *userEmail;
@end