//
//  UIViewController+BaseFunctionality.h
//  Bisqit
//
//  Created by Bohdan Orlov on 7/11/13.
//  Copyright (c) 2013 sjinnovation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BaseFunctionality)<CLLocationManagerDelegate>
-(void) setRightBarButtonItems;
-(void) setLeftbarButtonItems;

@end
