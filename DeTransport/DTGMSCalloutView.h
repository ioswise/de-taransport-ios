//
//  DTGMSCalloutView.h
//  DeTransport
//
//  Created by  Igor Cherepanov on 5/2/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MarqueeLabel.h"
#import "CBAutoScrollLabel.h"

@interface DTGMSCalloutView : UIView
@property (strong, nonatomic) IBOutlet UIButton *favoriteButton;
@property (strong, nonatomic) IBOutlet UIButton *listButton;
@property (strong, nonatomic) IBOutlet UIButton *mapButton;
@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *titleLabel;
-(id)initWithFrame:(CGRect)frame;
@end
