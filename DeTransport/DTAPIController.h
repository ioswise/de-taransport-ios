//
//  DTAPIController.h
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//
#import <AFHTTPSessionManager.h>
#import <AFNetworkActivityIndicatorManager.h>

typedef void (^SuccessBlock)(NSURLSessionTask *task, id responseObject);
typedef void (^FailureBlock)(NSURLSessionTask *task, NSError *error);

@interface DTAPIController : AFHTTPSessionManager


@property (strong, nonatomic) NSURLSessionTask *lastOperation;

+ (instancetype)sharedClient;

-(void)stopsListSuccess:(SuccessBlock)success
                   failure:(FailureBlock)failure;
-(void)vehiclesListForBusStopwithID:(int)ID success:(SuccessBlock)success
                            failure:(FailureBlock)failure;

@end
