//
//  main.m
//  DeTransport
//
//  Created by Dmytro Genyk on 3/6/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DTAppDelegate class]));
    }
}
