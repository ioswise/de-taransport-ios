//
//  DTAppDelegate.m
//  DeTransport
//
//  Created by Dmytro Genyk on 3/6/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTAppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Constants.h"
#import "GAI.h"
#import "DTViewController.h"

@implementation DTAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [DTDataStorage sharedStorage];
    [DTDataStorage unarchive];
    [DTDataStorage sharedStorage].allowedRadius = 600;
    [DTDataStorage sharedStorage].updateTimeInterval = 10;
    [DTDataStorage sharedStorage].shouldUseOnlineMaps = YES;

    
    if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0)
    {
        [[UIToolbar appearance] setBackgroundImage:[self imageWithColor:[Decorator colorWithHexString:@"5DA6A6"]] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
        [[UIToolbar appearance] setTintColor:[Decorator colorWithHexString:@"5DA6A6"]];
    }
    else
    {
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [[UISearchBar appearance] setTintColor:[UIColor whiteColor]];
        [[UITabBar appearance] setTintColor:[Decorator colorWithHexString:@"5DA6A6"]];
//        [[UITabBar appearance] setBackgroundColor:[UIColor colorWithWhite:1.000 alpha:0.300]];
    }
    [[UINavigationBar appearance] setTintColor:[Decorator colorWithHexString:@"5DA6A6"]];
    [[UINavigationBar appearance] setBackgroundImage:[self imageWithColor:[Decorator colorWithHexString:@"485F73"]] forBarMetrics:UIBarMetricsDefault];
//    [[UISearchBar appearance] setSearchFieldBackgroundImage:[self imageWithColor:[Decorator colorWithHexString:kBackgroundColor]] forState:UIControlStateNormal];
    [[UISearchBar appearance] setBackgroundImage:[self imageWithColor:[Decorator colorWithHexString:@"5DA6A6"]]];
//    [[UIImage imageNamed:@"backgrounNormalSegmentedControlItem.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 4, 0)];
    
    [GMSServices provideAPIKey:@"AIzaSyDQ0nf25EUKK4o-eU2D5S9XiDurqGH3RJo"];
    // Override point for customization after application launch.
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-57860155-1"];
    
//    [Crashlytics startWithAPIKey:@"060d6d84296395ed067dbb69c94e3154d6b52b7d"];
        
    // Check whether the language code has already been set.
        
    return YES;
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 44.0f, 26.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler
{
    DTViewController *rootVC = (DTViewController *)self.window.rootViewController;
    if ([shortcutItem.type isEqualToString:@"sjinnovation.com.DeTransport.map"])
        [rootVC setSelectedIndex:0];
    if ([shortcutItem.type isEqualToString:@"sjinnovation.com.DeTransport.bus-stop"])
        [rootVC setSelectedIndex:1];
    if ([shortcutItem.type isEqualToString:@"sjinnovation.com.DeTransport.details"])
        [rootVC setSelectedIndex:2];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [DTDataStorage archive];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [DTDataStorage archive];

    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
