//
//  DTListTableViewController.m
//  DeTransport
//
//  Created by  Igor Cherepanov on 3/28/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTListTableViewController.h"
#import "DTListTableViewCell.h"
#import "DTBusStop.h"

@interface DTListTableViewController (){
    NSMutableSet *lettersSet;
    NSArray *sortedSetArray;
    NSMutableDictionary *stopsBySections;
}

@end

@implementation DTListTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    stopsBySections = [NSMutableDictionary dictionary];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self splitByLetter];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName
           value:@"List Table view"];
    
    // manual screen tracking
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [lettersSet allObjects].count;
}
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    
    return sortedSetArray;
    
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
//    NSInteger newRow = [self indexForFirstChar:title inArray:[DTDataStorage sharedStorage].busStops];
//    NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:newRow inSection:0];
//    [tableView scrollToRowAtIndexPath:newIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
    
    return index;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [sortedSetArray objectAtIndex:section];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *letter = [sortedSetArray objectAtIndex:section];
    NSUInteger count = 0;
    NSMutableArray *array = [NSMutableArray array];
    for (DTBusStop *stop in [DTDataStorage sharedStorage].busStops) {
        if ([[stop comparableName] hasPrefix:letter]) {
            [array addObject:stop];
            count++;
        }
    }
    [stopsBySections setValue:array forKey:letter];
//    NSLog([NSString stringWithFormat:@"%d  %@ count:%d",section ,letter,count]);
    return count;
}


- (DTListTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DTListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listCell" forIndexPath:indexPath];
    
    NSString *letter = [sortedSetArray objectAtIndex:indexPath.section];
    NSMutableArray *array = [stopsBySections valueForKey:letter];
    DTBusStop *stop = [array objectAtIndex:indexPath.row];
    [cell.starImageView setImage:[UIImage imageNamed:@"fullStar"]];
    if ([[[DTDataStorage sharedStorage] favoriteStops] indexOfObject:stop] != NSNotFound) {
        [cell.starImageView setHidden:NO];
    }
    else{
        [cell.starImageView setHidden:YES];

    }
    cell.nameLabel.text = stop.name;
    
    // Configure the cell...
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DTListTableViewCell *cell = (DTListTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    NSString *letter = [sortedSetArray objectAtIndex:indexPath.section];
    NSMutableArray *array = [stopsBySections valueForKey:letter];
    DTBusStop *stop = [array objectAtIndex:indexPath.row];
    if (cell.starImageView.hidden) {
        [[[DTDataStorage sharedStorage] favoriteStops] addObject:stop];
    }
    else {
        [[[DTDataStorage sharedStorage] favoriteStops] removeObject:stop];
    }

    [cell.starImageView setHidden:!cell.starImageView.hidden];
    
}
#pragma mark Utils

-(void)splitByLetter{
    lettersSet = [[NSMutableSet alloc] init];
    for ( DTBusStop *stop in [DTDataStorage sharedStorage].busStops )
    {
        if ( [stop comparableName].length > 0 )
            [lettersSet addObject:[[stop comparableName] substringToIndex:1]];
    }
    sortedSetArray = [[lettersSet allObjects] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
}
- (NSInteger)indexForFirstChar:(NSString *)character inArray:(NSArray *)array
{
    NSUInteger count = 0;
    for (DTBusStop *stop in array) {
        if ([[stop comparableName] hasPrefix:character]) {
            return count;
        }
        count++;
    }
    return 0;
}



@end
