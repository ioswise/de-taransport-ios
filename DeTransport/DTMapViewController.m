//
//  DTMapViewController.m
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTMapViewController.h"
#import "DTBusStop.h"
//#import <MarqueeLabel.h>
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h> 
#import <objc/runtime.h>
#import "Constants.h"

@interface DTMapViewController (){
    GMSMapView *onlineMapView;
    id currentMapView;
    CLLocation *location;
    NSMutableArray *stopsToshow;
    UITapGestureRecognizer *tapRecognizer;
    int infoWindowWidth;
    int infoWindowHeight;
    float anchorSize;

}
@property (nonatomic) Reachability *internetReachability;
@property (strong, nonatomic) NSMutableArray *filteredArray;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) UIImage *myLocationImage;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) GMSMarker *pointMarker;
@property (nonatomic, assign) BOOL needShowMyLocation;
@property (nonatomic, assign) BOOL usingOnlineMaps;
@property (nonatomic, assign) BOOL needMoveToShowAllMarkers;

@end

@implementation DTMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.myLocationButton setImage:[UIImage imageNamed:@"location"] forState:UIControlStateNormal];
    infoWindowHeight = 50;
    infoWindowWidth = 300;
    anchorSize = 5;
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    
    self.searchTextField.layer.sublayerTransform = CATransform3DMakeTranslation(3, 0, 0);
    [[DTDataStorage sharedStorage] loadStops];
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(performSearch:)];
    self.filteredArray = [NSMutableArray array];
    stopsToshow = [NSMutableArray array];
    [self.myLocationButton.layer setCornerRadius:4.0];
    [self.myLocationButton.layer setBorderWidth:1.0f];
    [self.myLocationButton.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    if ([DTDataStorage sharedStorage].latestLocation != nil) {
        location = [DTDataStorage sharedStorage].latestLocation;
    }
    else{
        location = [[CLLocation alloc] initWithLatitude:49.55 longitude:25.585];
    }
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;

    // Do any additional setup after loading the view.
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMapType:) name:@"mapTypeChanged" object:nil];
    [self.mapContainerView addSubview:currentMapView];
    [self.tabBarController.tabBar setBackgroundColor:[UIColor colorWithWhite:1.000 alpha:0.670]];
//    self.myLocationImage = ;
    [self.searchTextField.layer setBorderColor:[UIColor colorWithWhite:0.521 alpha:1.000].CGColor];
    [self.searchTextField.layer setBorderWidth:1.0f];
    [self.searchTextField.layer setCornerRadius:5.0f];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateLocation) userInfo:nil repeats:YES];
    UIDevice *device = [UIDevice currentDevice];
    [device beginGeneratingDeviceOrientationNotifications];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:device];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
}
- (void)orientationChanged:(NSNotification *)note
{    
    if ([DTDataStorage sharedStorage].shouldUseOnlineMaps && self.internetReachability.currentReachabilityStatus != NotReachable) {
        onlineMapView.frame = self.view.frame;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    
    [DTDataStorage sharedStorage].shouldUseOnlineMaps = YES;
    [self changeMapType:nil];
    [super viewWillAppear:animated];
    self.screenName = @"Map View";
}

-(void)updateLocation
{
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager startUpdatingLocation];
        [self.locationManager startUpdatingHeading];
    }
//    [self.locationManager startUpdatingLocation];
//    [self.locationManager startUpdatingHeading];
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    }
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
//    if (self.needShowMyLocation)
//    {
//        if ([DTDataStorage sharedStorage].shouldUseOnlineMaps)
//        {
//            
//            [onlineMapView animateToLocation:self.locationManager.location.coordinate];
//        }
//        else
//        {
//            [offlineMapView setCenterCoordinate:self.locationManager.location.coordinate animated:YES];
//        }
//        self.needShowMyLocation = NO;
//    }
}
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    CLLocationDirection updatedHeading = newHeading.magneticHeading;
//    float headingFloat = 0 - newHeading.magneticHeading;
    self.pointMarker.map = nil;

    self.myLocationImage = [self imageRotatedByDegrees:[UIImage imageNamed:@"myLocation"] deg:updatedHeading];
    if ([DTDataStorage sharedStorage].shouldUseOnlineMaps)
    {
        self.pointMarker = [GMSMarker markerWithPosition:self.locationManager.location.coordinate];
        self.pointMarker.icon = self.myLocationImage;
        self.pointMarker.groundAnchor = CGPointMake(0.5, 0.5);
        self.pointMarker.map = onlineMapView;
    }
    
    [self.locationManager stopUpdatingHeading];
    [self.locationManager stopUpdatingLocation];
}


- (UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees{
    // calculate the size of the rotated view's containing box for our drawing space
    CGImageRef imgRef = oldImage.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    //    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0, width, height)];
    CGSize rotatedSize = CGSizeMake(width, height);
    // Create the bitmap context
    UIGraphicsBeginImageContextWithOptions(rotatedSize, NO, 0.0);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, (degrees * M_PI / 180));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-oldImage.size.width / 2, -oldImage.size.height / 2, oldImage.size.width, oldImage.size.height), [oldImage CGImage]);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.locationManager stopUpdatingHeading];
    [self.locationManager stopUpdatingLocation];
    [currentMapView removeFromSuperview];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Map Methods

-(void) changeMapType:(NSNotification *)notification{
    [self.locationManager stopUpdatingHeading];
    [self.locationManager stopUpdatingLocation];
    CGRect frame;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f)
        frame = self.view.frame;
    else
    {
        frame = CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height);
    }
    if ([DTDataStorage sharedStorage].shouldUseOnlineMaps && self.internetReachability.currentReachabilityStatus != NotReachable) {
//        if ([DTDataStorage sharedStorage].shouldUseOnlineMaps) {
        //online map
        if (onlineMapView == nil) {
       
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:13] ;
            onlineMapView = [GMSMapView mapWithFrame:frame camera:camera];
            [onlineMapView setMinZoom:10 maxZoom:16];
            UIView *notTappableView = [[UIView alloc] initWithFrame:CGRectMake(onlineMapView.frame.origin.x, onlineMapView.frame.size.height - 40, 70, 40)];
            [onlineMapView addSubview:notTappableView];
//            [notTappableView setUserInteractionEnabled:NO];
            onlineMapView.settings.rotateGestures = NO;
            onlineMapView.settings.tiltGestures = NO;
//            onlineMapView.settings.myLocationButton = YES;
//            onlineMapView.myLocationEnabled = YES;
            [onlineMapView setDelegate:self];
        }
//        [offlineMapView removeConstraints:offlineMapView.constraints];
        
//        offlineMapView = nil;
        self.mapPlaceholder.hidden = YES;
        
        self.usingOnlineMaps = YES;
        currentMapView = onlineMapView;
    }
    else {
        NSLog(@"user offline");
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
        
        if (onlineMapView)
        {
            self.mapPlaceholder.hidden = YES;
        }
        
        if([[[UIDevice currentDevice] systemVersion] floatValue]<8.0)
        {
            UIAlertView* curr1=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"noConnection", nil) message:NSLocalizedString(@"enableInSettings", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
            [curr1 show];
        }
        else
        {
            UIAlertView* curr2=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"noConnection", nil) message:NSLocalizedString(@"enableInSettings", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:NSLocalizedString(@"settings", nil), nil];
            curr2.tag=1001;
            [curr2 show];
        }
    }

    [self.mapContainerView addSubview:currentMapView];
//    [self configurePullabelView];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1001 && buttonIndex == 1)
    {
        // iOS 8 and later
        [[UIApplication sharedApplication] openURL:[NSURL  URLWithString:@"prefs:root"]];
    }
}

-(void)updateMap{
    
    self.needMoveToShowAllMarkers = YES;
    [onlineMapView setDelegate:self];
    GMSMapView *currentView = (GMSMapView *)currentMapView;
    [currentView clear];
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:location.coordinate coordinate:location.coordinate];
    for (DTBusStop *stop in stopsToshow) {
        GMSMarker *marker = [[GMSMarker alloc] init];
        UIImage *icon = [UIImage imageNamed:@"busStopIcon"];
        
        marker.icon = icon;
        marker.flat = NO;
        marker.position = CLLocationCoordinate2DMake(stop.lat.doubleValue, stop.lng.doubleValue);
        marker.title = stop.name;
        marker.map = currentView;
        marker.userData = stop;
        
        bounds = [bounds includingCoordinate:marker.position];

    }
    if (stopsToshow.count == 1) {
        DTBusStop *stop = stopsToshow.firstObject;
         [currentView animateToLocation:CLLocationCoordinate2DMake(stop.lat.doubleValue, stop.lng.doubleValue)];
    }
    else{
        if (self.needMoveToShowAllMarkers)
            [currentView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:15.0f]];
    }
    if (self.needShowMyLocation)
    {
        [currentView animateToLocation:self.locationManager.location.coordinate];
        self.needShowMyLocation = NO;
    }
    if (currentView.camera.zoom < currentView.minZoom)
        [currentView animateToZoom:currentView.minZoom - 1];
    
    
}

#pragma mark internet reachability

-(void)reachabilityChanged:(NSNotification *)note{
    NetworkStatus internetStatus = [self.internetReachability currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down. IN AGENDA");
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI.IN AGENDA");
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.IN AGENDA");
            [self changeMapType:nil];
            break;
        }
    }
}

//-(IBAction) toggleMaps:(id)sender
//{
//    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
//    switch (segmentedControl.selectedSegmentIndex) {
//            case 0:
//            [DTDataStorage sharedStorage].shouldUseOnlineMaps = NO;
//            break;
//            case 1:
//            [DTDataStorage sharedStorage].shouldUseOnlineMaps = YES;
//            break;
//    }
//    //[self changeMapType:nil];
//}

#pragma mark MapBox methods
//-(RMMapLayer *)mapView:(RMMapView *)mapView layerForAnnotation:(RMAnnotation *)annotation{
//    if ([annotation.annotationType isEqualToString:@"myLocation"])
//    {
//        RMMarker *marker;
//        marker = [[RMMarker alloc] initWithUIImage:self.myLocationImage];
//        return marker;
//    }
//    DTBusStop *stop = [annotation.userInfo valueForKey:@"stop"];
//    BOOL favorite;
//    RMMarker *marker = [[RMMarker alloc] initWithUIImage:[UIImage imageNamed:@"busStopIcon"]];
//    marker.anchorPoint = CGPointMake(0.5, 1);
//    marker.annotation = annotation;
//    marker.canShowCallout = YES;
//    marker.leftCalloutAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
//    marker.rightCalloutAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
//    UIButton *listButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 1, 30, 30)];
//    [listButton setImage:[UIImage imageNamed:@"listButton"] forState:UIControlStateNormal];
//    [listButton addTarget:self action:@selector(openVehicleList) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIButton *favoriteButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 1, 30, 30)];
//    objc_setAssociatedObject(favoriteButton, @"stop", stop, OBJC_ASSOCIATION_RETAIN);
//    
//    for (DTBusStop *favStop in [DTDataStorage sharedStorage].favoriteStops) {
//        if ([stop.name isEqualToString:favStop.name]) {
//            favorite = YES;
//            break;
//        }
//        else{
//            favorite = NO;
//        }
//    }
//    if (favorite) {
//        [favoriteButton setImage:[UIImage imageNamed:@"fullStar"] forState:UIControlStateNormal];
//        favoriteButton.selected = YES;
//    }
//    else{
//        [favoriteButton setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
//        favoriteButton.selected = NO;
//    }
//    [favoriteButton addTarget:self action:@selector(favouriteStop:) forControlEvents:UIControlEventTouchUpInside];
//    
//    
//    [marker.rightCalloutAccessoryView addSubview:listButton];
//    //[marker.rightCalloutAccessoryView addSubview:mapButton];
//    [marker.leftCalloutAccessoryView addSubview:favoriteButton];
//    
//    return marker;
//}
//-(void)singleTapOnMap:(RMMapView *)map at:(CGPoint)point
//{
//    for (RMAnnotation *annotation in map.annotations)
//    {
//        if (![annotation.annotationType isEqualToString:@"myLocation"])
//        {
//            RMMarker *marker = (RMMarker *)annotation.layer;
//            [marker replaceUIImage:[UIImage imageNamed:@"busStopIcon"]];
//            marker.anchorPoint = CGPointMake(0.5, 1);
//        }
//    }
//    [self.searchTextField resignFirstResponder];
//}
//-(void)tapOnAnnotation:(RMAnnotation *)annotation onMap:(RMMapView *)map
//{
//    for (RMAnnotation *annotation in map.annotations)
//    {
//        if (![annotation.annotationType isEqualToString:@"myLocation"])
//        {
//            RMMarker *marker = (RMMarker *)annotation.layer;
//            [marker replaceUIImage:[UIImage imageNamed:@"busStopIcon"]];
//            marker.anchorPoint = CGPointMake(0.5, 1);
//        }
//    }
//    if (![annotation.annotationType isEqualToString:@"myLocation"])
//    {
//        RMMarker *marker = (RMMarker *)annotation.layer;
//        [marker replaceUIImage:[UIImage imageNamed:@"selectedBusStopIcon"]];
//        marker.anchorPoint = CGPointMake(0.5, 1);
//        [map setCenterCoordinate:annotation.coordinate animated:YES];
//    }
//}
#pragma mark GoogleMaps methods

//- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
//    NSLog(@"view requested");
//
//    DTGMSCalloutView *view = [[DTGMSCalloutView alloc] initWithFrame:CGRectMake(0, 0, 300, 32)];
//    view.titleLabel.text = @"Place Name";
//    [view.listButton addTarget:self action:@selector(openVehicleList) forControlEvents:UIControlEventTouchUpInside];
//    return view;
//}
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    [self.searchTextField resignFirstResponder];
    [self.googleMapsCalloutView removeFromSuperview];
    
    if (marker == self.pointMarker)
        return nil;
    UIView *calloutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, infoWindowWidth, infoWindowHeight)];
    
//    float offset = anchorSize * M_SQRT2;
    
    self.googleMapsCalloutView = [[DTGMSCalloutView alloc] initWithFrame:CGRectMake(0, 0, infoWindowWidth, infoWindowHeight)];
    
    self.googleMapsCalloutView.layer.cornerRadius = 5;
    [self.googleMapsCalloutView.favoriteButton addTarget:self action:@selector(favouriteStop:) forControlEvents:UIControlEventTouchUpInside];
    DTBusStop *busStop = [marker userData];
    objc_setAssociatedObject(self.googleMapsCalloutView.favoriteButton, @"stop", busStop, OBJC_ASSOCIATION_RETAIN);
    BOOL favorite;
    for (DTBusStop *favStop in [DTDataStorage sharedStorage].favoriteStops) {
        if ([busStop.name isEqualToString:favStop.name]) {
            favorite = YES;
            break;
        }
        else{
            favorite = NO;
        }
    }
    if (favorite) {
        [self.googleMapsCalloutView.favoriteButton setImage:[UIImage imageNamed:@"fullStar"] forState:UIControlStateNormal];
        self.googleMapsCalloutView.favoriteButton.selected = YES;
    }
    else{
        [self.googleMapsCalloutView.favoriteButton setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
        self.googleMapsCalloutView.favoriteButton.selected = NO;
    }
    
    self.googleMapsCalloutView.titleLabel.text = busStop.name;
    [self.googleMapsCalloutView.listButton addTarget:self action:@selector(openVehicleList) forControlEvents:UIControlEventTouchUpInside];

    //[self.googleMapsCalloutView.mapButton addTarget:self action:@selector(onButton3Clicked) forControlEvents:UIControlEventTouchUpInside];
    
//    CLLocationCoordinate2D anchor = [onlineMapView.selectedMarker position];
//    CGPoint point = [onlineMapView.projection pointForCoordinate:anchor];
//    point.y -= onlineMapView.selectedMarker.icon.size.height + offset/2 + (infoWindowHeight - offset/2)/2;
//    self.googleMapsCalloutView.center = point;
    
    CGFloat x = self.googleMapsCalloutView.frame.size.width/2;
    CGFloat y = self.googleMapsCalloutView.frame.size.height;
    CGPoint A = CGPointMake(x - 10, y - 1);
    CGPoint B = CGPointMake(x + 10, y - 1);
    CGPoint C = CGPointMake(x, y + 12);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path,NULL,A.x,A.y);
    CGPathAddLineToPoint(path, NULL, B.x, B.y);
    CGPathAddLineToPoint(path, NULL, C.x, C.y);
    CGPathCloseSubpath(path);
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setPath:path];
    [shapeLayer setFillColor:[[UIColor whiteColor] CGColor]];
    [shapeLayer setBounds:CGRectMake(0.0f, 0.0f, 20, 15)];
    [shapeLayer setAnchorPoint:CGPointMake(0.0f, 0.0f)];
    [shapeLayer setPosition:CGPointMake(0, 0)];
    [[self.googleMapsCalloutView layer] addSublayer:shapeLayer];
    
    CGPathRelease(path);
    
    [onlineMapView addSubview:self.googleMapsCalloutView];
    return calloutView;
    
}
-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
//    mapView.selectedMarker.icon = [UIImage imageNamed:@"busStopIcon"];
//    [self.googleMapsCalloutView removeFromSuperview];
//    [self.searchTextField resignFirstResponder];
//    self.needMoveToShowAllMarkers = YES;
//    self.needShowMyLocation = YES;
//    [self performSearch:nil];
}
-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    if (marker == self.pointMarker)
        return NO;
    mapView.selectedMarker.icon = [UIImage imageNamed:@"busStopIcon"];
    mapView.selectedMarker = marker;
    if ([self.searchTextField isFirstResponder]) {
        [self performSearch:nil];
        return NO;
    }
    else{
        [mapView animateToLocation:marker.position];
        [self normalizeeMarker];
        marker.icon = [UIImage imageNamed:@"selectedBusStopIcon"];
        
        //        DTBusStop *stop = (DTBusStop *)marker.userData;
        //Handle marker tap
        return YES;
    }
    //    NSLog(@"taped");
}
- (void)mapView:(GMSMapView *)pMapView didChangeCameraPosition:(GMSCameraPosition *)position {
    if (pMapView.selectedMarker != nil && self.googleMapsCalloutView.superview) {
        [self normalizeeMarker];
    } else {
        [self.googleMapsCalloutView removeFromSuperview];
    }
}

-(void) normalizeeMarker
{
    CLLocationCoordinate2D anchor = [onlineMapView.selectedMarker position];
    CGPoint point = [onlineMapView.projection pointForCoordinate:anchor];
    float offset = anchorSize * M_SQRT2;
    point.y -= onlineMapView.selectedMarker.icon.size.height + offset/2 + (infoWindowHeight - offset/2)/2 + 10;
    self.googleMapsCalloutView.center = point;
}


- (void)favouriteStop:(id)sender {
    UIButton *button = sender;
    button.selected = !button.selected;
    DTBusStop *stop = objc_getAssociatedObject(sender, @"stop");
//    DTBusStop *stop = [array objectAtIndex:indexPath.row];
    if (button.selected) {
        [button setImage:[UIImage imageNamed:@"fullStar"] forState:UIControlStateNormal];
        [[[DTDataStorage sharedStorage] favoriteStops] addObject:stop];
    }
    else {
        [button setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
        [[[DTDataStorage sharedStorage] favoriteStops] removeObject:stop];
    }
//    NSLog(@"button 1");
    
}
- (void)details {
//    NSLog(@"button 2");
    onlineMapView.selectedMarker = nil;

}
- (void)onButton3Clicked {
    onlineMapView.selectedMarker = nil;
//    NSLog(@"button 3");
    
}
- (void)performSearch:(id)sender {
    if (stopsToshow.count == 0)
    {
        if ([self.searchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
            self.searchTextField.text = @"";
        [self.searchTextField resignFirstResponder];
        return;
    }
    if ([sender isKindOfClass:[DTBusStop class]]) {
        //sender is a stop
        DTBusStop *stop = (DTBusStop *)sender;
        [stopsToshow removeAllObjects];
        [stopsToshow addObject:stop];
    }
    [currentMapView removeGestureRecognizer:tapRecognizer];
    [self.filteredArray removeAllObjects];
    [self.suggestionTableView reloadData];
    [self updateMap];
}
-(void)openVehicleList{
    [self.tabBarController setSelectedIndex:2];
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
            if (onlineMapView.selectedMarker)
            {
                onlineMapView.selectedMarker.icon = [UIImage imageNamed:@"busStopIcon"];
                DTBusStop *stop = [onlineMapView.selectedMarker userData];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"selectedNerbyStop" object:nil userInfo:@{@"selectedStop": stop}];
                onlineMapView.selectedMarker = nil;
            }
    });

   }

#pragma mark TextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [[DTDataStorage sharedStorage] loadStops];
    [currentMapView addGestureRecognizer:tapRecognizer];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([currentMapView gestureRecognizers].count == 0) {
        [currentMapView addGestureRecognizer:tapRecognizer];

    }
    if ([textField.text stringByAppendingString:string].length >= 3)
    {
        // create a predicate. In my case, attivita is a property of the object stored inside the array
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@", [textField.text stringByAppendingString:string]];
        self.filteredArray = [[[DTDataStorage sharedStorage].busStops filteredArrayUsingPredicate:predicate] mutableCopy];
        stopsToshow = [self.filteredArray mutableCopy];
        if (stopsToshow.count > 0)
            [self updateMap];
    } else {
        [self.filteredArray removeAllObjects];
    }
    
    // reload section with fade animation
    [self.suggestionTableView reloadData];
    return YES;

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self performSearch:nil];
    return YES;
}
#pragma mark TableView Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int count = (int)self.filteredArray.count;
    CGRect frame = self.suggestionTableView.frame;
    tableView.hidden = NO;

    if (count > 5) {
        frame.size.height = 40 * 5;
    }
    else if (count == 0){
        frame.size.height = 1;
        tableView.hidden = YES;
    }
    else{
        frame.size.height = 40 * count;
    }
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        self.suggestionTableView.frame = frame;
        [UIView commitAnimations];
    return count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = [(DTBusStop *)[self.filteredArray objectAtIndex:indexPath.row] name];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DTBusStop *stop = (DTBusStop *)[self.filteredArray objectAtIndex:indexPath.row];
    self.searchTextField.text = stop.name;
    [self performSearch:stop];
}

- (IBAction)relocateToMyLocationWithStops:(id)sender
{
    self.searchTextField.text = @"";
    self.needShowMyLocation = YES;
    [[DTDataStorage sharedStorage].stopsNearby removeAllObjects];
    [DTDataStorage sharedStorage].latestLocation = self.locationManager.location;
    CLLocation *currentLocation = [DTDataStorage sharedStorage].latestLocation;
    NSMutableArray *array = [[DTDataStorage sharedStorage] busStops];
    for (DTBusStop *busStop in array){
        float dx = busStop.lat.floatValue - (float)currentLocation.coordinate.latitude;
        dx = fabs(dx);
        float dy = busStop.lng.floatValue - (float)currentLocation.coordinate.longitude;
        dy = fabs(dy);
        
        float distance = sqrtf(dx*dx + dy*dy);
        int distanceInMeters = distance * 100000;
        //        NSLog([NSString stringWithFormat:@"dist %d",distanceInMeters]);
        if (distanceInMeters < [DTDataStorage sharedStorage].allowedRadius) {
            //            NSLog(@"WIN!");
            //            NSLog([NSString stringWithFormat:@"Name: %@ \r Distance:%d meters",busStop.name, distanceInMeters]);
//            objc_setAssociatedObject(busStop, @"distance", [NSNumber numberWithInt:distanceInMeters], OBJC_ASSOCIATION_RETAIN);
            [[DTDataStorage sharedStorage].stopsNearby addObject:busStop];
        }
        stopsToshow = [DTDataStorage sharedStorage].stopsNearby;
    }
    if (stopsToshow.count == 0 && [DTDataStorage sharedStorage].allowedRadius <= 1000)
    {
        [DTDataStorage sharedStorage].allowedRadius = [DTDataStorage sharedStorage].allowedRadius + 100;
        [self relocateToMyLocationWithStops:sender];
    }
    else
    {
        [DTDataStorage sharedStorage].allowedRadius = 600;
        [self updateMap];
    }
}
//[yourMapView removeAnnotation:yourMapView.annotations];


@end
