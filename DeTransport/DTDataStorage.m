//
//  DTDataStorage.m
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTDataStorage.h"
#import "DTBusStop.h"
@implementation DTDataStorage
static DTDataStorage *sharedStorage = nil;

+ (DTDataStorage *)sharedStorage{
    @synchronized(self) {
        if (!sharedStorage)
        {
            sharedStorage = [[self alloc] init];
            sharedStorage.busStops = [NSMutableArray array];
            sharedStorage.stopsNearby = [NSMutableArray array];
            sharedStorage.favoriteStops = [NSMutableArray array];

            sharedStorage.allowedRadius = 600;
            sharedStorage.updateTimeInterval = 10;
            sharedStorage.shouldUseOnlineMaps = YES;
            sharedStorage.latestLocation = nil;
        }
    }
    return sharedStorage;
}
+ (void) sort{
    [[DTDataStorage sharedStorage].busStops sortUsingComparator:^NSComparisonResult(DTBusStop *obj1, DTBusStop *obj2) {
        NSString *name1, *name2;
        
        name1 = [obj1 comparableName];
        name2 = [obj2 comparableName];
        
        return  [name1 caseInsensitiveCompare:name2];
    }];
}
- (id)initWithCoder:(NSCoder *) aDecoder{
    DTDataStorage *instance = [DTDataStorage sharedStorage];
    
    [instance setBusStops:[aDecoder decodeObjectForKey:@"dataStorageBusStops"]];
    [instance setFavoriteStops:[aDecoder decodeObjectForKey:@"dataStorageFavoriteStops"]];
    
    [instance setAllowedRadius:[aDecoder decodeIntForKey:@"dataStorageAllowedRadius"]];
    [instance setUpdateTimeInterval:[aDecoder decodeIntForKey:@"dataStorageUpdateTime"]];
    [instance setShouldUseOnlineMaps:[aDecoder decodeBoolForKey:@"dataStorageOnlineMaps"]];
    
    [instance setLastUpdateDate:[aDecoder decodeObjectForKey:@"dataStorageLatestUpdate"]];


    return instance;

}
- (void)encodeWithCoder:(NSCoder *) aCoder{
    // Archive the singleton instance.
    DTDataStorage *instance = [DTDataStorage sharedStorage];
    
    [aCoder encodeObject:[instance busStops] forKey:@"dataStorageBusStops"];
    [aCoder encodeObject:[instance favoriteStops] forKey:@"dataStorageFavoriteStops"];

    [aCoder encodeInt:[instance allowedRadius] forKey:@"dataStorageAllowedRadius"];
    [aCoder encodeInt:[instance updateTimeInterval] forKey:@"dataStorageUpdateTime"];
    [aCoder encodeBool:[instance shouldUseOnlineMaps] forKey:@"dataStorageOnlineMaps"];
    
    [aCoder encodeObject:[instance lastUpdateDate] forKey:@"dataStorageLatestUpdate"];
    
}
-(void)loadStops{
    NSDate *date = [NSDate date];
    if([DTDataStorage sharedStorage].busStops.count == 0 || [date timeIntervalSinceDate:[[DTDataStorage sharedStorage] lastUpdateDate]] > 60 * 60 * 24){
        [[DTAPIController sharedClient] stopsListSuccess:^(NSURLSessionTask *task, id responseObject) {
            [[DTDataStorage sharedStorage].busStops removeAllObjects];
            for (id item in [responseObject valueForKey:@"stops"]) {
                DTBusStop *busStop = [DTBusStop busStopWithJSON:item];
                [[DTDataStorage sharedStorage].busStops addObject:busStop];
            }
            [DTDataStorage sort];
            [DTDataStorage sharedStorage].lastUpdateDate = date;
            
        } failure:^(NSURLSessionTask *task, NSError *error) {
            
        }];
        
    }

}
+ (void) archive{
    NSString *reminderEventIDsPathString = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"ReminderIDs.archive"];
    [NSKeyedArchiver archiveRootObject:[DTDataStorage sharedStorage] toFile:reminderEventIDsPathString];
}
+ (void) unarchive{
    NSString *reminderEventIDsPathString = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:@"ReminderIDs.archive"];
    DTDataStorage *tmp = [NSKeyedUnarchiver unarchiveObjectWithFile:reminderEventIDsPathString];
    if (tmp != nil) {
        sharedStorage = tmp;
    }
}
+ (void)eraseAll{
    NSString *p = [[[NSBundle mainBundle] bundlePath] stringByDeletingLastPathComponent];
    for (NSString *fname in @[ @"tmp", @"Library", @"Documents", @"Caches", @"Preferences" ]) {
        NSString *path = [p stringByAppendingPathComponent:fname];
        [[NSFileManager defaultManager] removeItemAtPath:path error:NULL];
    }
    
    sharedStorage.busStops = [NSMutableArray array];
    sharedStorage.favoriteStops = [NSMutableArray array];
    sharedStorage.allowedRadius = 600;
    sharedStorage.updateTimeInterval = 10;
    sharedStorage.shouldUseOnlineMaps = YES;
    sharedStorage.latestLocation = nil;

    [DTDataStorage archive];
}

@end
