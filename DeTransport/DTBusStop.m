//
//  DTBusStop.m
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTBusStop.h"

@implementation DTBusStop

+(DTBusStop *)busStopWithJSON:(id)JSON
{
    return [[self alloc] initWithJSON:JSON];
}
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"])
        self.ID = value;
}
-(NSString *)comparableName{
    int firstNumberIndex, firstLetterIndex, index;
    firstLetterIndex = (int)[self.name rangeOfCharacterFromSet:[NSCharacterSet uppercaseLetterCharacterSet]].location;
    
    firstNumberIndex = (int)[self.name rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location;
    
    if (firstLetterIndex == -1) {
        index = firstNumberIndex;
    }
    else if(firstNumberIndex == -1){
        index = firstLetterIndex;
    }
    else{
        index = firstNumberIndex > firstLetterIndex ? firstLetterIndex : firstNumberIndex;
    }
    return [self.name substringFromIndex:index];
}

- (id)initWithCoder:(NSCoder *) aDecoder{
    DTBusStop *instance = [[DTBusStop alloc] init];
    
    [instance setID:[aDecoder decodeObjectForKey:@"dataStorageBusID"]];
    [instance setName:[aDecoder decodeObjectForKey:@"dataStorageStopName"]];
    [instance setLat:[aDecoder decodeObjectForKey:@"dataStorageStopLat"]];
    [instance setLng:[aDecoder decodeObjectForKey:@"dataStorageStopLng"]];


    return instance;
    
}
- (void)encodeWithCoder:(NSCoder *) aCoder{
    // Archive the singleton instance.
    DTBusStop *instance = self;
    
    [aCoder encodeObject:[instance ID] forKey:@"dataStorageBusID"];
    [aCoder encodeObject:[instance name] forKey:@"dataStorageStopName"];
    [aCoder encodeObject:[instance lat] forKey:@"dataStorageStopLat"];
    [aCoder encodeObject:[instance lng] forKey:@"dataStorageStopLng"];

    
    
}
@end
