//
//  DTAppDelegate.h
//  DeTransport
//
//  Created by Dmytro Genyk on 3/6/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
