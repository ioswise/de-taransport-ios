//
//  DTListTableViewController.h
//  DeTransport
//
//  Created by  Igor Cherepanov on 3/28/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DTListTableViewController : UITableViewController <UIAlertViewDelegate>

@end
