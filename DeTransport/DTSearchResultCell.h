//
//  DTSearchResultCell.h
//  DeTransport
//
//  Created by Dmytro Genyk on 3/11/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTVehicle.h"

@interface DTSearchResultCell : UITableViewCell
@property (strong, nonatomic) DTVehicle *vehicle;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bortLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
-(void) configureCellWithVehicle:(DTVehicle *)vehicle;

@end
