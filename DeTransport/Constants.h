
#ifndef Constants_h
#define Constants_h

extern NSString *const kIconColor;
extern NSString *const kBackgroundColor;
extern NSString *const kGreyColor;
extern NSString *const kLightGreenColor;
extern NSString *const kDarkGreenColor;

extern NSString *const kOrangeColor;

extern NSString *const kLiveURL;

#define DEFAULTS_KEY_LANGUAGE_CODE @"LanguageCode" //NSUserDefaults key. The key against which to store the selected language code.

//Custom localised string macro, functioning in a similar way to the standard NSLocalisedString().
#define CustomLocalisedString(key, comment) \
[[DTLanguageManager sharedLanguageManager] getTranslationForKey:key]

#endif
