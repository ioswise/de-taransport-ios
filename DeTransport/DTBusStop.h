//
//  DTBusStop.h
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "JSONObject.h"

@interface DTBusStop : JSONObject <NSCoding>
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;

+ (DTBusStop *)busStopWithJSON:(id)JSON;
- (NSString *)comparableName;

- (id)initWithCoder:(NSCoder *) aDecoder;
- (void)encodeWithCoder:(NSCoder *) aCoder;

@end
