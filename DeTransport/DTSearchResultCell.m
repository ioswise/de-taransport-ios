//
//  DTSearchResultCell.m
//  DeTransport
//
//  Created by Dmytro Genyk on 3/11/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTSearchResultCell.h"

@implementation DTSearchResultCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)configureCellWithVehicle:(DTVehicle *)vehicle{
    if (vehicle.type == 3) {
        [self.iconImageView setImage:[UIImage imageNamed:@"socBus.png"]];
    }
    if (vehicle.type == 2) {
        [self.iconImageView setImage:[UIImage imageNamed:@"trolybus.png"]];
    }
    if (vehicle.type == 1) {
        [self.iconImageView setImage:[UIImage imageNamed:@"bus.png"]];
    }
    self.nameLabel.text = vehicle.name;
    self.bortLabel.text = vehicle.bortNumber;
    if (vehicle.time < 60) {
        self.timeLabel.text = @"1 хв";
    }
    else{
        self.timeLabel.text = [NSString stringWithFormat:@"%d хв",vehicle.time / 60];
    }
    self.distanceLabel.text = [NSString stringWithFormat:@"%.1f км", vehicle.distance/1000.f];

}
@end
