//
//  DTAPIController.m
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTAPIController.h"

@implementation DTAPIController

+ (instancetype)sharedClient {
    static DTAPIController *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[DTAPIController alloc] initWithBaseURL:[NSURL URLWithString:kLiveURL]];
        sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
        
    });
    
    return sharedClient;
}
-(void)stopsListSuccess:(SuccessBlock)success
                   failure:(FailureBlock)failure
{
        NSString *path = [NSString stringWithFormat:@"/stops/list"];
    [self GET:path parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            success(task, responseObject);
        } failure:^(NSURLSessionTask *task, NSError *error) {
            failure(task,error);
        }];
}
-(void)vehiclesListForBusStopwithID:(int)ID success:(SuccessBlock)success failure:(FailureBlock)failure{
    NSString *path = [NSString stringWithFormat:@"/vehicles/info"];
    NSDictionary *params = @{@"stop": [NSNumber numberWithInt:ID]};
    [self POST:path parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        success(task, responseObject);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        failure(task,error);
    }];
}
@end
