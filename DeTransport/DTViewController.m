//
//  DTViewController.m
//  DeTransport
//
//  Created by iOS Developer on 8/12/15.
//  Copyright (c) 2015 Dmytro Genyk. All rights reserved.
//

#import "DTViewController.h"
#import "DTMapViewController.h"
#import "DTFavoritesViewController.h"
#import "DTDetailViewController.h"
//#import "DTSettingsViewController.h"

@interface DTViewController ()

@end

@implementation DTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    for (UIViewController *vc in self.viewControllers) {
        if ([self.viewControllers indexOfObject:vc] == 0)
        {
            DTMapViewController *mapVC = (DTMapViewController *)vc;
            mapVC.tabBarItem.image = [UIImage imageNamed:@"globeButton"];
        }
        else if ([self.viewControllers indexOfObject:vc] == 1)
        {
            DTFavoritesViewController *favoritesVC = (DTFavoritesViewController *)vc;
            favoritesVC.tabBarItem.image = [UIImage imageNamed:@"star"];
        }
        else if ([self.viewControllers indexOfObject:vc] == 2)
        {
            DTDetailViewController *detailVC = (DTDetailViewController *)vc;
            detailVC.tabBarItem.image = [UIImage imageNamed:@"listButton"];
        }
//        else if ([self.viewControllers indexOfObject:vc] == 3)
//        {
//            DTSettingsViewController *settingsVC = (DTSettingsViewController *)vc;
//            settingsVC.tabBarItem.image = [UIImage imageNamed:@"settings"];
//        }
    }
    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
