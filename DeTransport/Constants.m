
#import "Constants.h"

NSString *const kIconColor = @"3cbbc4";
NSString *const kBackgroundColor = @"F2EAD0";
NSString *const kOrangeColor = @"DD6B4C";
NSString *const kGreyColor = @"B2B2B2";
NSString *const kLightGreenColor = @"3EBBA7";
NSString *const kDarkGreenColor = @"00747A";

NSString *const kLiveURL = @"http://api.detransport.com.ua";

