//
//  DTBusStopCell.h
//  DeTransport
//
//  Created by  Igor Cherepanov on 3/19/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTBusStopCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@end
