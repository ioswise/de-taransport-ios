//
//  DTBusPin.h
//  DeTransport
//
//  Created by  Igor Cherepanov on 3/17/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "DTVehicle.h"

@interface DTBusPin : NSObject<MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;

+(DTBusPin *)pinWithVehicle:(DTVehicle *)vehicle;
@end
