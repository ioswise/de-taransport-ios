//
//  DTFavoritesViewController.m
//  DeTransport
//
//  Created by  Igor Cherepanov on 3/25/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTFavoritesViewController.h"
#import "DTBusStop.h"
#import "Constants.h"
@interface DTFavoritesViewController (){
    BOOL isEditing;
}

@end

@implementation DTFavoritesViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//    [self.tableView setBackgroundColor:[Decorator colorWithHexString:kBackgroundColor]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void)viewWillAppear:(BOOL)animated{
    [DTDataStorage archive];
    [self.tableView reloadData];
    
    self.screenName = @"Favorites";
}
-(void)viewWillDisappear:(BOOL)animated{
    if (isEditing) {
        [self editFavorites:nil];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([DTDataStorage sharedStorage].favoriteStops.count > 0)
    {
        self.editBarButton.enabled = YES;
        return [DTDataStorage sharedStorage].favoriteStops.count;
    }
    else
    {
        [tableView setEditing:NO animated:YES];
        
        self.editBarButton.title = NSLocalizedString(@"edit", nil);
        self.editBarButton.enabled = NO;
        UIBarButtonItem *addBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addFavorites:)];
        self.addBarButton = addBarButton;
        self.navigationItem.rightBarButtonItem = self.addBarButton;
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UIImageView *upArrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"upArrow"]];
    [upArrowImageView setTag:15];
    if ([DTDataStorage sharedStorage].favoriteStops.count > 0)
    {
        DTBusStop *busStop = [[DTDataStorage sharedStorage].favoriteStops objectAtIndex:indexPath.row];
        cell.textLabel.text = busStop.name;
        cell.accessoryType = UITableViewCellAccessoryNone;
        for (UIView *subview in cell.contentView.subviews)
        {
            if (subview.tag == 15)
                [subview removeFromSuperview];
        }
    }
    else
    {
        upArrowImageView.frame = CGRectMake(cell.frame.size.width - 39, 2, 30, 30);
        upArrowImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        cell.textLabel.text = NSLocalizedString(@"addFavorite", nil);
        [cell.contentView addSubview:upArrowImageView];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    // Configure the cell...
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([DTDataStorage sharedStorage].favoriteStops.count > 0)
    {
        DTBusStop *busStop = [[DTDataStorage sharedStorage].favoriteStops objectAtIndex:indexPath.row];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"selectedNerbyStop" object:nil userInfo:@{@"selectedStop": busStop}];
        UINavigationController *navController = self.tabBarController.viewControllers[2];
        DTSearchViewController *searchViewController = navController.viewControllers[0];
        searchViewController.currentStop = busStop;
        [self.tabBarController setSelectedIndex:2];
    }
}

- (IBAction)editFavorites:(id)sender {
    
    isEditing = !isEditing;
    if (isEditing) {
        
        UIBarButtonItem *clearAllButton;
        
        clearAllButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"clearAll", nil)
                                                          style:UIBarButtonItemStylePlain
                                                         target:self
                                                         action:@selector(clearAllFavorites:)];
        self.editBarButton.title = NSLocalizedString(@"done", nil);
        
        self.navigationItem.rightBarButtonItem = clearAllButton;
        [self.tableView setEditing:YES animated:YES];
    }
    else{
        
        self.editBarButton.title = NSLocalizedString(@"edit", nil);
        
        UIBarButtonItem *addBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addFavorites:)];
        self.addBarButton = addBarButton;
        self.navigationItem.rightBarButtonItem = self.addBarButton;

        [self.tableView setEditing:NO animated:YES];

    }
}
- (IBAction)addFavorites:(id)sender {
    [self performSegueWithIdentifier:@"openList" sender:self];
}

- (IBAction)clearAllFavorites:(id)sender {
    UIAlertView *alert;
    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"clearAll", nil)
                                       message:NSLocalizedString(@"uncheckVerification", nil)
                                      delegate:self
                             cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                             otherButtonTitles:NSLocalizedString(@"clear", nil) , nil];
    [alert show];
    [self.tableView setEditing:NO animated:YES];
}
-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        return;
    }
    [[[DTDataStorage sharedStorage] favoriteStops] removeAllObjects];
    [self.tableView reloadData];
    [self editFavorites:nil];

}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([DTDataStorage sharedStorage].favoriteStops.count > 0)
    {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}


- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    
    return proposedDestinationIndexPath;

}
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    DTBusStop *busStop = [[DTDataStorage sharedStorage].favoriteStops objectAtIndex:sourceIndexPath.row];
    [[DTDataStorage sharedStorage].favoriteStops removeObjectAtIndex:sourceIndexPath.row];
    [[DTDataStorage sharedStorage].favoriteStops insertObject:busStop atIndex:destinationIndexPath.row];
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if ([DTDataStorage sharedStorage].favoriteStops.count == 1)
        {
            [[DTDataStorage sharedStorage].favoriteStops removeObjectAtIndex:indexPath.row];
            [tableView setEditing:NO animated:YES];
            [tableView reloadData];
        }
        else if ([DTDataStorage sharedStorage].favoriteStops.count > 1)
        {
            [tableView beginUpdates];
            [[DTDataStorage sharedStorage].favoriteStops removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView endUpdates];
        }
    }
}
@end
