//
//  DTSearchViewController.m
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTSearchViewController.h"
#import "DTVehicle.h"
#import "DTDetailViewController.h"

@interface DTSearchViewController (){
    BOOL loading;
}
@property (nonatomic, strong) NSMutableArray *filteredArray;
@property (nonatomic, strong) EGORefreshTableHeaderView *refreshView;

@end

@implementation DTSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarItem.image = [UIImage imageNamed:@"listButton"];
    
    self.stops = [NSMutableArray array];
    self.vehicles = [NSMutableArray array];
    self.filteredArray = [NSMutableArray array];
    [self setRightBarButtonItems];
    [self setLeftbarButtonItems];
    
    [self.sortingSegmentedControl setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.sortingSegmentedControl setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [self.sortingSegmentedControl setDividerImage:[[UIImage alloc] init] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self.sortingSegmentedControl setBackgroundColor:[Decorator colorWithHexString:@"5DA6A6"]];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont systemFontOfSize:15.0f], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName,
                                [NSValue valueWithCGPoint:CGPointZero], UITextAttributeTextShadowOffset,
                                nil];
    [self.sortingSegmentedControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [UIFont boldSystemFontOfSize:17.0f], NSFontAttributeName,
                                           [UIColor blackColor], NSForegroundColorAttributeName,
                                           [NSValue valueWithCGPoint:CGPointZero], UITextAttributeTextShadowOffset,
                                           nil];
    [self.sortingSegmentedControl setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];

    self.resultTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//    [self.resultTableView setBackgroundColor:[Decorator colorWithHexString:kBackgroundColor]];
    if (self.currentStop)
    {
        [self loadVehiclesForStop:self.currentStop];
    }

    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTintColor:[UIColor lightGrayColor]];
    
//    self.searchBar.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    [[DTDataStorage sharedStorage] loadStops];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectNerbyStop:) name:@"selectedNerbyStop" object:nil];
	// Do any additional setup after loading the view.
//    UITapGestureRecognizer *dismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
//    [self.resultTableView addGestureRecognizer:dismiss];
    self.searchResultTableView.layer.borderWidth = 1.0;
    self.searchResultTableView.layer.borderColor = [UIColor blackColor].CGColor;
}
//-(void)dismissKeyboard
//{
//    [self.searchBar resignFirstResponder];
//}
-(void)viewDidAppear:(BOOL)animated{
    if (self.currentStop)
    {
        [Decorator MBProgressHUDShowLoading];
        [self loadVehiclesForStop:self.currentStop];
    }
    if (self.refreshView == nil) {
        self.refreshView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.resultTableView.bounds.size.height, self.view.frame.size.width, self.resultTableView.bounds.size.height)];
        self.refreshView.delegate = self;
        [self.resultTableView addSubview:self.refreshView];
    }
    self.screenName = @"Search";
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.resultTableView reloadData];
    loading = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchResultTableView)
    {
        if (self.filteredArray.count != 0)
        {
            [self.searchResultTableView setHidden:NO];
            
            // 42 -> searchTableViewCell height, 168 - > searchTableViewHeight
            if (self.filteredArray.count < 4)
            {
                self.searchHeightTableView.constant = self.filteredArray.count * 42;
            }
            else {
                self.searchHeightTableView.constant = 168;
            }
            
            return self.filteredArray.count;
        }
        else
        {
            [self.searchResultTableView setHidden:YES];
            return 0;
        }
    }
    else
    {
        return self.vehicles.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchResultTableView)
    {
        DTSearchSuggestionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchSuggestionCell" forIndexPath:indexPath];
        cell.busStopLabel.text = [(DTBusStop *)[self.filteredArray objectAtIndex:indexPath.row] name];
        return cell;

    }
    else
    {
        DTSearchResultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchResultCell" forIndexPath:indexPath];
        DTVehicle *vehicle = [self.vehicles objectAtIndex:indexPath.row];
        cell.vehicle = vehicle;
        [cell configureCellWithVehicle:vehicle];

        return cell;

    }
    // Configure the cell...
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.searchBar resignFirstResponder];
    if (tableView == self.searchResultTableView)
    {
        self.currentStop = [self.filteredArray objectAtIndex:indexPath.row];
        [self loadVehiclesForStop:self.currentStop];

    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:YES];
    [[DTDataStorage sharedStorage] loadStops];
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO];
    [searchBar resignFirstResponder];

    [self.filteredArray removeAllObjects];
    [self.searchResultTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;
{
    if (![searchText isEqualToString:@""]) // here you check that the search is not null, if you want add another check to avoid searches when the characters are less than 3
    {
        // create a predicate. In my case, attivita is a property of the object stored inside the array
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@", searchText];
        self.filteredArray = [[[DTDataStorage sharedStorage].busStops filteredArrayUsingPredicate:predicate] mutableCopy];
    } else {
        [self.filteredArray removeAllObjects];
    }
    
    // reload section with fade animation
    [self.searchResultTableView reloadData];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"vehicleDetail"]) {
        [self searchBarCancelButtonClicked:self.searchBar];
        DTDetailViewController *detailController = (DTDetailViewController *)segue.destinationViewController;
        DTSearchResultCell *cell = (DTSearchResultCell *)sender;
        detailController.vehicle = cell.vehicle;
        detailController.currentStop = self.currentStop;
    }
}
-(void)loadVehiclesForStop:(DTBusStop *) stop{
    self.currentStop = stop;
    self.searchBar.text = self.currentStop.name;
    [[DTAPIController sharedClient] vehiclesListForBusStopwithID:self.currentStop.ID.intValue success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(responseObject);
        self.vehicles = [NSMutableArray array];
        for (id JSON in [responseObject valueForKey:@"vehicles"]) {
            DTVehicle *vehicle = [DTVehicle vehicleWithJSON:JSON];
            vehicle.number = [vehicle.name intValue];
            [self.vehicles addObject:vehicle];
        }
        if (self.vehicles.count != 0)
        {
            if (self.sortingSegmentedControl.selectedSegmentIndex == -1)
                [self.sortingSegmentedControl setSelectedSegmentIndex:3];
            NSString *sortDescriptorKey;
            switch (self.sortingSegmentedControl.selectedSegmentIndex) {
                case 0:
                    sortDescriptorKey = @"type";
                    break;
                case 1:
                    sortDescriptorKey = @"number";
                    break;
                case 2:
                    sortDescriptorKey = @"time";
                    break;
                case 3:
                    sortDescriptorKey = @"distance";
                    break;
                default:
                    break;
            }

            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortDescriptorKey
                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray;
            sortedArray = [self.vehicles sortedArrayUsingDescriptors:sortDescriptors];
            
            self.vehicles = [sortedArray mutableCopy];
        }
        [self searchBarCancelButtonClicked:self.searchBar];
        [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.5];
    } failure:^(NSURLSessionTask *task, NSError *error) {
        [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:0.5];

//        NSLog(@"fail");
    }];

}

- (IBAction)sortBy:(id)sender {
    [self.searchBar resignFirstResponder];
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    NSString *sortDescriptorKey;
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            sortDescriptorKey = @"type";
            break;
        case 1:
            sortDescriptorKey = @"number";
            break;
        case 2:
            sortDescriptorKey = @"time";
            break;
        case 3:
            sortDescriptorKey = @"distance";
            break;
        default:
            break;
    }
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortDescriptorKey
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [self.vehicles sortedArrayUsingDescriptors:sortDescriptors];
    
    self.vehicles = [sortedArray mutableCopy];
    [self.resultTableView reloadData];
}

#pragma mark - Refresh view
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.searchBar resignFirstResponder];
	[self.refreshView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[self.refreshView egoRefreshScrollViewDidEndDragging:scrollView];
	
}

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    [self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return loading; // should return if data source model is reloading
	
}
- (void)reloadTableViewDataSource{
    [self loadVehiclesForStop:self.currentStop];
    loading = YES;
}

- (void)doneLoadingTableViewData{
    
    [Decorator MBProgressHUDHideAllHUDs];
	//  model should call this when its done loading
    [self.resultTableView reloadData];

	loading = NO;
	[self.refreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.resultTableView];
	
}

@end
