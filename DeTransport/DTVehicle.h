//
//  DTVehicle.h
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "JSONObject.h"

@interface DTVehicle : JSONObject
@property (strong, nonatomic) NSString *bortNumber;
@property (strong, nonatomic) NSString *nextBortNumber;
@property (nonatomic) int distance;
@property (nonatomic) int nextDistance;
@property (nonatomic) int angle;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (strong, nonatomic) NSString *name;
@property (nonatomic) int time;
@property (nonatomic) int number;
@property (nonatomic) int nextTime;
@property (nonatomic) int type;
+(DTVehicle *) vehicleWithJSON:(id)JSON;
-(UIImage *)iconForVehicle;
@end
