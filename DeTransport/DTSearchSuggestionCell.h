//
//  DTSearchSuggestionCell.h
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTSearchSuggestionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *busStopLabel;

@end
