//
//  DTSearchViewController.h
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTBusStop.h"
#import "DTSearchSuggestionCell.h"
#import "DTSearchResultCell.h"
#import "EGORefreshTableHeaderView.h"
#import "GAITrackedViewController.h"


@interface DTSearchViewController : GAITrackedViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, EGORefreshTableHeaderDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *searchResultTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchHeightTableView;
@property (weak, nonatomic) IBOutlet UITableView *resultTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortingSegmentedControl;

@property (nonatomic, strong) NSMutableArray *stops;
@property (nonatomic, strong) NSMutableArray *vehicles;
@property (nonatomic, strong) DTBusStop *currentStop;
-(void)loadVehiclesForStop:(DTBusStop *) stop;

- (IBAction)sortBy:(id)sender;


@end
