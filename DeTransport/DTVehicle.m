//
//  DTVehicle.m
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTVehicle.h"

@implementation DTVehicle
+(DTVehicle *)vehicleWithJSON:(id)JSON
{
    return [[self alloc] initWithJSON:JSON];
}
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"bort_number"])
        self.bortNumber = value;
    if ([key isEqualToString:@"bort_numbernext"])
        self.nextBortNumber = value;
    if ([key isEqualToString:@"distance"])
            self.distance = [(NSNumber *)value intValue];
    if ([key isEqualToString:@"distancenext"])
        self.nextDistance = [(NSNumber *)value intValue];
    if ([key isEqualToString:@"lat"])
        self.latitude = [(NSNumber *)value floatValue];
    if ([key isEqualToString:@"lng"])
        self.longitude = [(NSNumber *)value floatValue];
    if ([key isEqualToString:@"time"])
        self.time = [(NSNumber *)value intValue];
    if ([key isEqualToString:@"timenext"])
        self.nextTime = [(NSNumber *)value intValue];
    if ([key isEqualToString:@"type"])
        self.type = [(NSNumber *)value intValue];

}
-(UIImage *)iconForVehicle{
    UIImage *pinImage;
    switch (self.type) {
        case 1:
            pinImage = [UIImage imageNamed:@"busOnMap"];
            break;
        case 2:
            pinImage = [UIImage imageNamed:@"trolybusOnMap"];
            break;
        case 3:
            pinImage = [UIImage imageNamed:@"socBusOnMap"];
            break;
            
        default:
            break;
    }
    return pinImage;

}
@end
