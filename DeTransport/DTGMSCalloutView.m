//
//  DTGMSCalloutView.m
//  DeTransport
//
//  Created by  Igor Cherepanov on 5/2/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTGMSCalloutView.h"

@implementation DTGMSCalloutView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        self.favoriteButton = [[UIButton alloc] initWithFrame:CGRectMake(2, 11, 28, 28)];
        [self.favoriteButton setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
//        self.titleLabel =[[MarqueeLabel alloc] initWithFrame:CGRectMake(32, 0, frame.size.width - 96, 32) rate:200.0f andFadeLength:5.0f];
//        
//        self.titleLabel.numberOfLines = 1;
//        self.titleLabel.opaque = NO;
//        self.titleLabel.enabled = YES;
//        self.titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
//        self.titleLabel.textAlignment = NSTextAlignmentLeft;
//        self.titleLabel.textColor = [UIColor whiteColor];
//        self.titleLabel.backgroundColor = [UIColor clearColor];
//        self.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.000];
        
        self.titleLabel = [[CBAutoScrollLabel alloc] initWithFrame:CGRectMake(32, 9, frame.size.width - 67, 32)];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.labelSpacing = 20; // distance between start and end labels
        self.titleLabel.pauseInterval = 2.0f; // seconds of pause before scrolling starts again
        self.titleLabel.scrollSpeed = 40.0f; // pixels per second
        self.titleLabel.textAlignment = NSTextAlignmentLeft; // centers text when no auto-scrolling is applied
        self.titleLabel.fadeLength = 5.f; // length of the left and right edge fade, 0 to disable
        
        self.listButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width - 34, 11, 28, 28)];
        [self.listButton setImage:[UIImage imageNamed:@"listButton"] forState:UIControlStateNormal];
        //self.mapButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width - 32, 1, 28, 28)];
        //[self.mapButton setImage:[UIImage imageNamed:@"globeButton"] forState:UIControlStateNormal];
        //[self.mapButton addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.favoriteButton];
        [self addSubview:self.titleLabel];
        [self addSubview:self.listButton];
        //[self addSubview:self.mapButton];
    }
    return self;
}

@end
