//
//  DTFavoritesViewController.h
//  DeTransport
//
//  Created by  Igor Cherepanov on 3/25/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTSearchViewController.h"
#import "GAITrackedViewController.h"

@interface DTFavoritesViewController : GAITrackedViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editBarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addBarButton;

@end
