//
//  Decorator.m
//  TPT
//
//  Created by Bohdan Orlov on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "Decorator.h"
#import "Constants.h"

@implementation Decorator


+ (UIColor *)colorWithHexString:(NSString *)hex {
    return [self colorWithHexString:hex alpha:1.0];
}
+ (UIColor*)colorWithHexString:(NSString*)hex alpha:(float)alpha{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:alpha];
    
}
+(void)MBProgressHUDShowSuccessWithStatus:(NSString *)status
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] windows] lastObject] animated:YES];
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"success"]];
    [HUD.customView setFrame:CGRectMake(0, 0, 50, 50)];
    HUD.detailsLabelFont = [UIFont boldSystemFontOfSize:14];
    HUD.detailsLabelText = status;
    [HUD show:YES];
	[HUD hide:YES afterDelay:1.5];
}
+(void)MBProgressHUDShowLoading
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] windows] lastObject] animated:YES];
//    HUD.mode = MBProgressHUDModeAnnularDeterminate;
    HUD.detailsLabelText = NSLocalizedString(@"loading", nil);
    
//    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
//    if ([language isEqualToString:@"uk"])
//    {
//        HUD.detailsLabelText = @"Оновлення...";
//    }
//    else
//    {
//        HUD.detailsLabelText = @"Loading...";
//    }
    [HUD show:YES];
}
+(void)MBProgressHUDShowErrorWithStatus:(NSString *)status
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] windows] lastObject] animated:YES];
    HUD.mode = MBProgressHUDModeCustomView;
//    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error"]];
    HUD.detailsLabelFont = [UIFont boldSystemFontOfSize:14];
    HUD.detailsLabelText = status;
    [HUD show:YES];
	[HUD hide:YES afterDelay:3];
}

+(void)MBProgressHUDHideAllHUDs
{
    [MBProgressHUD hideAllHUDsForView:[[[UIApplication sharedApplication] windows] lastObject] animated:YES];
}

@end
