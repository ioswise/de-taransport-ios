//
//  DTDataStorage.h
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface DTDataStorage : NSObject <NSCoding>

@property (nonatomic, strong) NSMutableArray *busStops;

@property (nonatomic, strong) CLLocationManager *manager;
@property (nonatomic) int allowedRadius;
@property (nonatomic) int updateTimeInterval;
@property (nonatomic) BOOL shouldUseOnlineMaps;
@property (nonatomic, strong) CLLocation *latestLocation;


@property (nonatomic, strong) NSMutableArray *stopsNearby;
@property (nonatomic, strong) NSMutableArray *favoriteStops;
@property (nonatomic, strong) NSDate *lastUpdateDate;



+ (DTDataStorage *)sharedStorage;
+ (void) sort;

- (id)initWithCoder:(NSCoder *) aDecoder;
- (void)encodeWithCoder:(NSCoder *) aCoder;
- (void)loadStops;

+ (void) archive;
+ (void) unarchive;
+ (void)eraseAll;

@end
