//
//  DTMapViewController.h
//  DeTransport
//
//  Created by Dmytro Genyk on 3/10/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "DTGMSCalloutView.h"
#import "GAITrackedViewController.h"

@interface DTMapViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, GMSMapViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *mapContainerView;
@property (weak, nonatomic) IBOutlet UITableView *suggestionTableView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet DTGMSCalloutView *googleMapsCalloutView;
@property (weak, nonatomic) IBOutlet UIButton *myLocationButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceToMyLocationButton;
@property (weak, nonatomic) IBOutlet UIImageView *mapPlaceholder;

- (IBAction)relocateToMyLocationWithStops:(id)sender;
@end
