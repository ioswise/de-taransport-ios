
//
//  UIViewController+BaseFunctionality.m
//  Bisqit
//
//  Created by Bohdan Orlov on 7/11/13.
//  Copyright (c) 2013 sjinnovation. All rights reserved.
//

#import "UIViewController+BaseFunctionality.h"
//#import "DTSettingsViewController.h"
#import "DTSearchViewController.h"
#import <objc/runtime.h>
#import "Constants.h"


@implementation UIViewController (BaseFunctionality)
-(void) setRightBarButtonItems
{
//    UIBarButtonItem *settingsBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings.png"] landscapeImagePhone:nil style:UIBarButtonItemStylePlain target:self action:@selector(settings:)];
//    
//    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects: settingsBarButton, nil];
}
-(void) setLeftbarButtonItems
{
//    UIBarButtonItem *locationBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"location.png"] landscapeImagePhone:nil style:UIBarButtonItemStylePlain target:self action:@selector(locate:)];
//    if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0)
//        [locationBarButton setWidth:45];
//    else
//        [locationBarButton setWidth:33];
    UIButton *locationBarButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage *backBtnImage = [UIImage imageNamed:@"location.png"]  ;
    [locationBarButton setBackgroundImage:backBtnImage forState:UIControlStateNormal];
    [locationBarButton addTarget:self action:@selector(locate:) forControlEvents:UIControlEventTouchUpInside];
    locationBarButton.frame = CGRectMake(0, 0, 33, 33);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:locationBarButton] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
//    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects: locationBarButton, nil];
}


-(IBAction)locate:(id)sender{
    [[DTDataStorage sharedStorage] loadStops];
    if ([self respondsToSelector:@selector(stops)]) {
        if(![DTDataStorage sharedStorage].manager){
            [DTDataStorage sharedStorage].manager = [[CLLocationManager alloc] init];
        }
        [DTDataStorage sharedStorage].manager.delegate = self;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
            [[DTDataStorage sharedStorage].manager requestWhenInUseAuthorization];
        [[DTDataStorage sharedStorage].manager startUpdatingLocation];
//        if (![CLLocationManager locationServicesEnabled]) {
//       
//        }

    }
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSString *message, *title, *button;
    
    title = NSLocalizedString(@"location", nil);
    message = NSLocalizedString(@"allowLocationServices", nil);
    button = NSLocalizedString(@"ok", nil);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:button otherButtonTitles:nil];
    [alert show];
    [manager stopUpdatingHeading];
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
        [manager stopUpdatingLocation];
    manager.delegate = nil;
    manager = nil;

    [[DTDataStorage sharedStorage].stopsNearby removeAllObjects];
    [DTDataStorage sharedStorage].latestLocation = (CLLocation *)locations.lastObject;
    CLLocation *currentLocation = (CLLocation *)locations.lastObject;
    NSMutableArray *array = [[DTDataStorage sharedStorage] busStops];
    for (DTBusStop *busStop in array){
        float dx = busStop.lat.floatValue - (float)currentLocation.coordinate.latitude;
        dx = fabs(dx);
        float dy = busStop.lng.floatValue - (float)currentLocation.coordinate.longitude;
        dy = fabs(dy);

        float distance = sqrtf(dx*dx + dy*dy);
        int distanceInMeters = distance * 100000;
//        NSLog([NSString stringWithFormat:@"dist %d",distanceInMeters]);
        if (distanceInMeters < [DTDataStorage sharedStorage].allowedRadius) {
//            NSLog(@"WIN!");
//            NSLog([NSString stringWithFormat:@"Name: %@ \r Distance:%d meters",busStop.name, distanceInMeters]);
            objc_setAssociatedObject(busStop, @"distance", [NSNumber numberWithInt:distanceInMeters], OBJC_ASSOCIATION_RETAIN);
            [[DTDataStorage sharedStorage].stopsNearby addObject:busStop];
        }

    }
    if (!self.presentedViewController) {
        if ([DTDataStorage sharedStorage].stopsNearby.count != 0)
        {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            {
                [self showNerbyBusStops:self.navigationItem.leftBarButtonItem];
            }
            else
            {
                [self showNerbyBusStops:nil];
            }
        }
        else
        {
            UIAlertView *errorAlerView;
            errorAlerView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"noStopsMessage", nil)
                                                       message:NSLocalizedString(@"increaseRadiusMessage", nil)
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"ok", nil)
                                             otherButtonTitles: nil];
            [errorAlerView show];
        }
    }
//    NSLog(@"end");

}
-(void) showNerbyBusStops:(UIBarButtonItem *)sender{
    UIStoryboard *storyboard ;
    storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UITableViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"busStopsPopUpController"];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if ([DTDataStorage sharedStorage].stopsNearby.count < 10)
        {
            controller.view.frame = CGRectMake(10, 100, 400, 44 * [DTDataStorage sharedStorage].stopsNearby.count); //44 cell heigt
        }
        else {
            controller.view.frame = CGRectMake(10, 100, 400, 440);
        }

        controller.tableView.layer.cornerRadius = 5.0;
//        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:controller];
//        
//        popover.popoverContentSize = CGSizeMake(320, 400);
//        [popover presentPopoverFromBarButtonItem:sender
//                        permittedArrowDirections:UIPopoverArrowDirectionAny
//                                        animated:YES];
////        [popover presentPopoverFromRect:CGRectMake(sender.frame.size.width / 2, sender.frame.size.height / 1, 1, 1) inView:sender permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        
        if ([DTDataStorage sharedStorage].stopsNearby.count < 5)
        {
            controller.view.frame = CGRectMake(10, 100, 300, 44 * [DTDataStorage sharedStorage].stopsNearby.count); //44 cell heigt
        }
        else {
            controller.view.frame = CGRectMake(10, 100, 300, 220);
        }

        controller.tableView.layer.cornerRadius = 5.0;
    }
    [self presentPopupViewController:controller animationType:MJPopupViewAnimationFade];
    
    
}
-(void) didSelectNerbyStop:(NSNotification *) notification{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [self.tabBarController setSelectedIndex:2];
    [self.navigationController popToRootViewControllerAnimated:NO];
    [(DTSearchViewController *)self loadVehiclesForStop:[notification.userInfo valueForKey:@"selectedStop"]];

}
@end
