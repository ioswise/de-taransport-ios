//
//  DTBusPin.m
//  DeTransport
//
//  Created by  Igor Cherepanov on 3/17/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTBusPin.h"
@implementation DTBusPin

+(DTBusPin *)pinWithVehicle:(DTVehicle *)vehicle{
    DTBusPin *pin = [[super alloc]init];
    pin.title = vehicle.name;
    pin.coordinate = CLLocationCoordinate2DMake(vehicle.latitude, vehicle.longitude);
    return pin;
    
}
@end
