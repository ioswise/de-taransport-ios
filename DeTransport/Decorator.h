//
//  Decorator.h
//  TPT
//
//  Created by Bohdan Orlov on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

/* This class is used to set all appearance for almost all elements. */

#import <Foundation/Foundation.h>

@interface Decorator : NSObject
+ (UIColor *)colorWithHexString:(NSString*)hex;
+ (UIColor *)colorWithHexString:(NSString *)hex alpha:(float)alpha;
+ (void) MBProgressHUDShowSuccessWithStatus:(NSString *)status;
+ (void) MBProgressHUDShowLoading;
+ (void) MBProgressHUDShowErrorWithStatus:(NSString *)status;
+ (void)MBProgressHUDHideAllHUDs;
@end
