//
//  DTDetailViewController.m
//  DeTransport
//
//  Created by  Igor Cherepanov on 3/17/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import "DTDetailViewController.h"
#import "DTBusPin.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Reachability.h"
#import <objc/runtime.h>
#import "Constants.h"

@interface DTDetailViewController (){
    NSTimer *updateTimer;
    GMSMapView *onlineMapView;
    id mapView;
    CLLocation *location;
    BOOL fullInfoOpen;

}
@property (nonatomic) Reachability *internetReachability;

@end

@implementation DTDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    fullInfoOpen = YES;
//    [self.view setBackgroundColor:[Decorator colorWithHexString:kBackgroundColor]];
    [self setRightBarButtonItems];
    location = [[CLLocation alloc] initWithLatitude:self.vehicle.latitude longitude:self.vehicle.longitude];
    [self loadData];
    
    NSString *type;
    
    if (self.vehicle.type == 1 ) {
        
        type = NSLocalizedString(@"bus", nil);
    }
    if (self.vehicle.type ==2 ) {
        
        type = NSLocalizedString(@"trolleybus", nil);
    }
    if (self.vehicle.type == 3) {
        
        type = NSLocalizedString(@"socBus", nil);
    }
    NSString *titleString;
    
    titleString = [NSString stringWithFormat: NSLocalizedString(@"%@ N%@", nil), type, self.vehicle.name];

    [self.navigationItem setTitle:titleString];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMapType:) name:@"mapTypeChanged" object:nil];

//    CALayer *infoLayer = self.infoView.layer;
//    infoLayer.masksToBounds = NO;
//    infoLayer.cornerRadius = 15.0;
//    infoLayer.shadowOffset = CGSizeMake(0.0, 3.0);
//    infoLayer.shadowRadius = 5.0;
//    infoLayer.shadowOpacity = 0.6;
//    
//    CALayer *mapLayer = self.containerForMapView.layer;
//    mapLayer.masksToBounds = NO;
//    mapLayer.cornerRadius = 15;
//    mapLayer.shadowOffset = CGSizeMake(0.0, -3.0);
//    mapLayer.shadowRadius = 5.0;
//    mapLayer.shadowOpacity = 0.6;
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
}
-(void)viewWillAppear:(BOOL)animated{
    updateTimer = [NSTimer scheduledTimerWithTimeInterval: [DTDataStorage sharedStorage].updateTimeInterval target: self
                                                 selector: @selector(update) userInfo: nil repeats: YES];
    [self changeMapType:nil];
    
    self.screenName = @"Details";

}
-(void)viewDidLayoutSubviews{
    [self.containerForMapView addSubview:mapView];
    [(UIView *)mapView setFrame:CGRectMake(0, 0, self.containerForMapView.frame.size.width, self.containerForMapView.frame.size.height)];
  //  UIGestureRecognizer *infoRecognizer = [[UIGestureRecognizer alloc] initWithTarget:self action:@selector(toggleFullInfo:)];
  //  [self.infoTouchView addGestureRecognizer:infoRecognizer];
  //  [self.mapTouchView addGestureRecognizer:infoRecognizer];

}
-(void)viewDidDisappear:(BOOL)animated{
    [updateTimer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadData{
//    self.numberLabel.text = self.vehicle.name;
//    self.bortLabel.text = self.vehicle.bortNumber;
//    self.nextBortLabel.text = self.vehicle.nextBortNumber;
    if (self.vehicle.time < 60) {
        self.timeLabel.text = NSLocalizedString(@"lessMinute", nil);
    }
    else{
        self.timeLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d minutes", nil), self.vehicle.time / 60];
    }
    if (self.vehicle.nextTime < 60) {
        self.nextTimeLabel.text = NSLocalizedString(@"lessMinute", nil);
    }
    else{
        self.nextTimeLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d minutes", nil), self.vehicle.nextTime / 60];
    }
    
    self.distanceLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d meters", nil), self.vehicle.distance];
    self.nextDistanceLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d meters", nil), self.vehicle.nextDistance];

    location = [[CLLocation alloc] initWithLatitude:self.vehicle.latitude longitude:self.vehicle.longitude];
    
    if(onlineMapView){
        [self updateOnlineMap];
    }
    

}
//-(void) toggleFullInfo:(UIGestureRecognizer *)a{
//    if (fullInfoOpen) { //show map
//        self.mapTouchView.hidden = YES;
//        self.infoTouchView.hidden = NO;
//        [UIView animateWithDuration:.25
//                         animations:^{
//                             self.mapToDistanceConstraint.constant = 135.0;
//                             [self.view layoutSubviews];
//
//                         }
//                         completion:^(BOOL finished){
//                             CALayer *mapLayer = self.containerForMapView.layer;
//                             mapLayer.masksToBounds = NO;
//                             mapLayer.cornerRadius = 15;
//                             mapLayer.shadowOffset = CGSizeMake(0.0, -3.0);
//                             mapLayer.shadowRadius = 5.0;
//                             mapLayer.shadowOpacity = 0.6;
//                             [UIView animateWithDuration:.25
//                                              animations:^{
//                                                  [self.view bringSubviewToFront:self.containerForMapView];
//                                                  [self.view bringSubviewToFront:self.mapTouchView];
//                                                  self.mapToDistanceConstraint.constant = 10.0;
//                                                  [self.view layoutSubviews];
//
//                                                  
//                                                  CGRect frame = self.mapTouchView.frame;
//                                                  frame.origin.x = 0;
//                                                  frame.origin.y = 0;
//                                                  [(UIView *)mapView setFrame:frame];
//                                                  [(UIView *)mapView setClipsToBounds:YES];
//                                                  [[(UIView *)mapView layer] setCornerRadius:15];
//
//                                              }
//                                              completion:^(BOOL finished){
//
//                                                 
////                                                  NSLog(@"completion block");
//                                              }];
//
//                         }];
//
//        fullInfoOpen = NO;
//    }
//    else{ //hide map
//        self.mapTouchView.hidden = NO;
//        self.infoTouchView.hidden = YES;
//        [UIView animateWithDuration:.25
//                         animations:^{
//                             self.mapToDistanceConstraint.constant = 135.0;
//                             [self.view layoutSubviews];
//                             
//                         }
//                         completion:^(BOOL finished){
//                             [UIView animateWithDuration:.25
//                                              animations:^{
//                                                  [self.view bringSubviewToFront:self.infoView];
//                                                  [self.view bringSubviewToFront:self.infoTouchView];
//                                                  
//                                                  self.mapToDistanceConstraint.constant = 100.0;
//                                                  [self.view layoutSubviews];
//                                                  
//                                                  
//                                                  CGRect frame = self.mapTouchView.frame;
//                                                  frame.origin.x = 0;
//                                                  frame.origin.y = 0;
//                                                  [(UIView *)mapView setFrame:frame];
//                                              }
//                                              completion:^(BOOL finished){
//
//                                                  
//                                                  CALayer *mapLayer = self.containerForMapView.layer;
//                                                  mapLayer.masksToBounds = NO;
//                                                  mapLayer.cornerRadius = 15;
//                                                  mapLayer.shadowOffset = CGSizeMake(0.0, -3.0);
//                                                  mapLayer.shadowRadius = 5.0;
//                                                  mapLayer.shadowOpacity = 0.6;
////                                                  NSLog(@"completion block");
//                                              }];
//                             
//                         }];
//
//       
//        fullInfoOpen = YES;
//    }
//}
-(void) update{
//    NSLog(@"fired");

    [[DTAPIController sharedClient] vehiclesListForBusStopwithID:[self.currentStop.ID intValue] success:^(NSURLSessionTask *task, id responseObject) {
        //NSLog(responseObject);
//        NSLog(@"good");

        BOOL busIsPresent = NO;
        DTVehicle *newVehicle;
        for (id JSON in [responseObject valueForKey:@"vehicles"]) {
            DTVehicle *vehicle = [DTVehicle vehicleWithJSON:JSON];
            if ([vehicle.bortNumber isEqualToString:self.vehicle.bortNumber]) {
                self.vehicle = vehicle;
                busIsPresent = YES;
                break;
            }
            else if ([self.vehicle.nextBortNumber isEqualToString:vehicle.bortNumber]){
                newVehicle = vehicle;
            }
        }
        if (!busIsPresent) {
            [updateTimer invalidate];
//            NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
//            UIAlertView *alert;
//            if ([language isEqualToString:@"uk"])
//                alert = [[UIAlertView alloc] initWithTitle:@"Ви пропустили автобус" message:@"Ваш автобус проїхав дану зупинку. Ми покажемо інформацю про наступний" delegate:self cancelButtonTitle:@"Добре" otherButtonTitles:nil];
//            else
//                alert = [[UIAlertView alloc] initWithTitle:@"Missed the bus" message:@"The bus you were tracking passed current stop. We will show info for next one" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [alert show];
            // TODO

            /*
 
 
            DELEGATE  S!!!!!!!!!
            
            
            
            
            */
//            NSLog(@"buss passed the stop");
            updateTimer = [NSTimer scheduledTimerWithTimeInterval: [DTDataStorage sharedStorage].updateTimeInterval target: self
                                                         selector: @selector(update) userInfo: nil repeats: YES];
            self.vehicle = newVehicle;
        }
        else {
            [self loadData];
        }
        
        
    } failure:^(NSURLSessionTask *task, NSError *error) {
//        NSLog(@"fail");
    }];

}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    updateTimer = [NSTimer scheduledTimerWithTimeInterval: [DTDataStorage sharedStorage].updateTimeInterval target: self
                                                 selector: @selector(update) userInfo: nil repeats: YES];
}

//-(void)updateOfflineMap{
//    RMMapView *currentView = (RMMapView *)mapView;
//    [currentView removeAnnotations:currentView.annotations];
//    RMAnnotation *annotation = [RMAnnotation annotationWithMapView:currentView coordinate:location.coordinate andTitle:nil];
//    annotation.userInfo = @{@"icon": [self.vehicle iconForVehicle]};
//    [currentView addAnnotation:annotation];
//    currentView.centerCoordinate = location.coordinate;
//}

-(void)updateOnlineMap{
    GMSMapView *currentView = (GMSMapView *)mapView;
    [currentView clear];
    GMSMarker *marker = [[GMSMarker alloc] init];
    UIImage *icon = [self.vehicle iconForVehicle];
    icon = [icon imageRotatedByDegrees:180+self.vehicle.angle];
    marker.icon = icon;
    marker.groundAnchor = CGPointMake(0.5, 0.5);
    marker.flat = YES;
    
    
    marker.position = location.coordinate;
    marker.map = currentView;

    //[currentView setCamera:[GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:onlineMapView.camera.zoom]];
    [currentView animateToLocation:location.coordinate];


    
}
-(void) changeMapType:(NSNotification *)notification{
    if ([DTDataStorage sharedStorage].shouldUseOnlineMaps && self.internetReachability.currentReachabilityStatus != NotReachable) {
        //online map
        if (onlineMapView == nil) {
            
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:13];
            onlineMapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.containerForMapView.frame.size.width, self.containerForMapView.frame.size.height) camera:camera];
        }
        [onlineMapView setMinZoom:16 maxZoom:16];
        
        mapView = onlineMapView;
    
        [self updateOnlineMap];
        
    }
    
    [self.containerForMapView addSubview:(UIView *)mapView];
}

#pragma mark Map delegate
//-(RMMapLayer *)mapView:(RMMapView *)mapView layerForAnnotation:(RMAnnotation *)annotation{
//
//    UIImage *icon = [annotation.userInfo valueForKey:@"icon"];
//    icon = [icon imageRotatedByDegrees:180+self.vehicle.angle];
//    RMMarker *marker = [[RMMarker alloc] initWithUIImage:icon];
//    marker.annotation = annotation;
//
//
//    return marker;
//}
//-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
//    MKAnnotationView *pin = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pin"];
//    pin.image = [self.vehicle iconForVehicle];
//    pin.transform = CGAffineTransformMakeRotation((180 + self.vehicle.angle) * M_PI / 180);
//    return pin;
//}
#pragma mark Gesture delegate

//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    UITouch *touch = [touches anyObject];
//    if(touch.view == self.mapTouchView || touch.view == self.infoTouchView){
//        [self toggleFullInfo:nil];
//    }
//}

@end
