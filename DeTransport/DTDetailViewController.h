//
//  DTDetailViewController.h
//  DeTransport
//
//  Created by  Igor Cherepanov on 3/17/14.
//  Copyright (c) 2014 Dmytro Genyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DTVehicle.h"
#import "DTBusStop.h"
#import "GAITrackedViewController.h"

@interface DTDetailViewController : GAITrackedViewController<UIAlertViewDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic) DTVehicle *vehicle;
@property (strong, nonatomic) DTBusStop *currentStop;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextDistanceLabel;
@property (weak, nonatomic) IBOutlet UIView *containerForMapView;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIView *mapTouchView;
@property (weak, nonatomic) IBOutlet UIView *infoTouchView;

@end
